package panadería;
import java.awt.Toolkit;
import java.awt.*;
import javax.swing.*;
import java.sql.*;
import javax.swing.table.DefaultTableModel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.filechooser.FileNameExtensionFilter;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Rafmar extends javax.swing.JFrame {

    Conexion c;
    Connection cn;
    
    public Rafmar() throws IOException {
        try { 
           Runtime.getRuntime().exec("C:\\xampp\\xampp_start.exe"); 
        } 
        catch (Exception e) {
            dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        }
        c = new Conexion();
        cn = c.connect();
        initComponents();
        setPaymentMethods();
        this.setLocationRelativeTo(null);
        this.setIconImage(Toolkit.getDefaultToolkit().getImage("Icon.png"));
        showStock();
        initReserve();
        lock(availables,reserved,price,product_name,registered_products,update);
        showReport();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabbed_pane = new javax.swing.JTabbedPane();
        reserves = new javax.swing.JPanel();
        client_label_r = new javax.swing.JLabel();
        client_name_r = new javax.swing.JTextField();
        products_combobox_r = new javax.swing.JComboBox<>();
        add = new javax.swing.JButton();
        payment_label_r = new javax.swing.JLabel();
        quantity_r = new javax.swing.JSpinner();
        reserve = new javax.swing.JButton();
        scroll_pane_r = new javax.swing.JScrollPane();
        products_list_r = new javax.swing.JTable();
        payment_method_r = new javax.swing.JComboBox<>();
        cancel = new javax.swing.JButton();
        total = new javax.swing.JLabel();
        quantity_label_r = new javax.swing.JLabel();
        payment_number_r = new javax.swing.JTextField();
        confirm_sale_r = new javax.swing.JCheckBox();
        confirm_sale = new javax.swing.JPanel();
        order_id_label = new javax.swing.JLabel();
        order_ids = new javax.swing.JTextField();
        payment_label_s = new javax.swing.JLabel();
        delete_reserve = new javax.swing.JButton();
        scroll_pane_sa = new javax.swing.JScrollPane();
        sales_list = new javax.swing.JTable();
        confirm_sale_button = new javax.swing.JButton();
        payment_number_s = new javax.swing.JTextField();
        search_for_transfers = new javax.swing.JPanel();
        scroll_pane_t = new javax.swing.JScrollPane();
        found_transfers = new javax.swing.JTable();
        transfers_since = new com.toedter.calendar.JDateChooser();
        transfers_until = new com.toedter.calendar.JDateChooser();
        transfers_until_label = new javax.swing.JLabel();
        transfers_since_label = new javax.swing.JLabel();
        search_for_file = new javax.swing.JButton();
        search_for_transfers_button = new javax.swing.JButton();
        reports = new javax.swing.JPanel();
        client_label_a = new javax.swing.JLabel();
        client_name_a = new javax.swing.JTextField();
        payment_method_a = new javax.swing.JComboBox<>();
        reports_until = new com.toedter.calendar.JDateChooser();
        reports_since = new com.toedter.calendar.JDateChooser();
        scroll_pane_a = new javax.swing.JScrollPane();
        sales_table = new javax.swing.JTable();
        search_reports = new javax.swing.JButton();
        stock = new javax.swing.JPanel();
        register_product = new javax.swing.JButton();
        registered_product = new javax.swing.JButton();
        availables_label = new javax.swing.JLabel();
        availables = new javax.swing.JTextField();
        update = new javax.swing.JButton();
        product_name_label = new javax.swing.JLabel();
        product_name = new javax.swing.JTextField();
        registered_products = new javax.swing.JComboBox<>();
        price_label = new javax.swing.JLabel();
        price = new javax.swing.JTextField();
        scroll_pane_st = new javax.swing.JScrollPane();
        products_list_s = new javax.swing.JTable();
        reserved = new javax.swing.JTextField();
        reserved_label = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("PANADERÍA RAFMAR, C. A.");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        reserves.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        client_label_r.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        client_label_r.setText("Cliente:");
        reserves.add(client_label_r, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 50, 30));

        client_name_r.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        client_name_r.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                client_name_rKeyTyped(evt);
            }
        });
        reserves.add(client_name_r, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 20, 150, 30));

        products_combobox_r.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        products_combobox_r.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                products_combobox_rActionPerformed(evt);
            }
        });
        reserves.add(products_combobox_r, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 60, 190, 30));

        add.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        add.setText("Agregar");
        add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addActionPerformed(evt);
            }
        });
        reserves.add(add, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 140, -1, 30));

        payment_label_r.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        payment_label_r.setText("Transacción #");
        reserves.add(payment_label_r, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 20, 90, 30));

        quantity_r.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        quantity_r.setModel(new javax.swing.SpinnerNumberModel(1, 1, null, 1));
        quantity_r.setName(""); // NOI18N
        reserves.add(quantity_r, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 140, 50, 30));

        reserve.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        reserve.setText("Aceptar");
        reserve.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reserveActionPerformed(evt);
            }
        });
        reserves.add(reserve, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 220, 90, 30));

        products_list_r.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        products_list_r.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Producto", "Precio unitario", "Cantidad", "Precio total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        products_list_r.setEnabled(false);
        scroll_pane_r.setViewportView(products_list_r);

        reserves.add(scroll_pane_r, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, 450, 190));

        payment_method_r.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        reserves.add(payment_method_r, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 20, 150, 30));

        cancel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        cancel.setText("Cancelar");
        cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelActionPerformed(evt);
            }
        });
        reserves.add(cancel, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 220, 90, 30));

        total.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        total.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        total.setText("Total:");
        reserves.add(total, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 180, 190, 30));

        quantity_label_r.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        quantity_label_r.setText("Cantidad:");
        reserves.add(quantity_label_r, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 140, -1, 30));

        payment_number_r.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        payment_number_r.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                payment_number_rKeyTyped(evt);
            }
        });
        reserves.add(payment_number_r, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 20, 160, 30));

        confirm_sale_r.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        confirm_sale_r.setText("Confirmar la venta");
        reserves.add(confirm_sale_r, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 100, 190, 30));

        tabbed_pane.addTab("Generar reservas/ventas", reserves);

        confirm_sale.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        order_id_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        order_id_label.setText("Órdenes #");
        confirm_sale.add(order_id_label, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 70, 30));

        order_ids.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        order_ids.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                order_idsActionPerformed(evt);
            }
        });
        order_ids.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                order_idsKeyTyped(evt);
            }
        });
        confirm_sale.add(order_ids, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 20, 130, 30));

        payment_label_s.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        payment_label_s.setText("Transacción #");
        confirm_sale.add(payment_label_s, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 20, 90, 30));

        delete_reserve.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        delete_reserve.setText("Eliminar");
        delete_reserve.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delete_reserveActionPerformed(evt);
            }
        });
        confirm_sale.add(delete_reserve, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 20, 100, 30));

        sales_list.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        sales_list.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Número de orden", "Fecha", "Cliente", "Método de pago", "Productos", "Precio total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        sales_list.setEnabled(false);
        scroll_pane_sa.setViewportView(sales_list);

        confirm_sale.add(scroll_pane_sa, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, 660, 190));

        confirm_sale_button.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        confirm_sale_button.setText("Confirmar");
        confirm_sale_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirm_sale_buttonActionPerformed(evt);
            }
        });
        confirm_sale.add(confirm_sale_button, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 20, 100, 30));

        payment_number_s.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        payment_number_s.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                payment_number_sKeyTyped(evt);
            }
        });
        confirm_sale.add(payment_number_s, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 20, 110, 30));

        tabbed_pane.addTab("Confirmar ventas", confirm_sale);

        search_for_transfers.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        found_transfers.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        found_transfers.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Fecha(s) de la(s) venta(s)", "Fecha de la transferencia", "Usuario(s)", "Referencia", "Precio esperado", "Precio encontrado"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        found_transfers.setEnabled(false);
        scroll_pane_t.setViewportView(found_transfers);
        if (found_transfers.getColumnModel().getColumnCount() > 0) {
            found_transfers.getColumnModel().getColumn(4).setResizable(false);
        }

        search_for_transfers.add(scroll_pane_t, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 660, 210));

        transfers_since.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        transfers_since.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                transfers_sincePropertyChange(evt);
            }
        });
        search_for_transfers.add(transfers_since, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 10, 130, 30));

        transfers_until.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        transfers_until.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                transfers_untilPropertyChange(evt);
            }
        });
        search_for_transfers.add(transfers_until, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 10, 130, 30));

        transfers_until_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        transfers_until_label.setText("Hasta:");
        search_for_transfers.add(transfers_until_label, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 10, 50, 30));

        transfers_since_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        transfers_since_label.setText("Desde:");
        search_for_transfers.add(transfers_since_label, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 10, 50, 30));

        search_for_file.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        search_for_file.setText("Buscar transferencias");
        search_for_file.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                search_for_fileActionPerformed(evt);
            }
        });
        search_for_transfers.add(search_for_file, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 10, 150, 30));

        search_for_transfers_button.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        search_for_transfers_button.setText("Buscar archivo");
        search_for_transfers_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                search_for_transfers_buttonActionPerformed(evt);
            }
        });
        search_for_transfers.add(search_for_transfers_button, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 110, 30));

        tabbed_pane.addTab("Buscar transferencias", search_for_transfers);

        reports.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        client_label_a.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        client_label_a.setText("Nombre:");
        reports.add(client_label_a, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 10, 50, 30));

        client_name_a.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        client_name_a.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                client_name_aKeyTyped(evt);
            }
        });
        reports.add(client_name_a, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 40, 130, 30));

        payment_method_a.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        payment_method_a.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                payment_method_aActionPerformed(evt);
            }
        });
        reports.add(payment_method_a, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 90, 130, 30));

        reports_until.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        reports_until.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                reports_untilPropertyChange(evt);
            }
        });
        reports.add(reports_until, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 180, 130, 30));

        reports_since.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        reports_since.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                reports_sincePropertyChange(evt);
            }
        });
        reports.add(reports_since, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 140, 130, 30));

        sales_table.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        sales_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Fecha", "Usuario", "Precio", "Método de pago", "Referencia", "Estado del pedido"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        sales_table.setEnabled(false);
        scroll_pane_a.setViewportView(sales_table);

        reports.add(scroll_pane_a, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 530, 250));

        search_reports.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        search_reports.setText("Buscar");
        search_reports.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                search_reportsActionPerformed(evt);
            }
        });
        reports.add(search_reports, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 230, 130, 30));

        tabbed_pane.addTab("Reportes", reports);

        stock.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        register_product.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        register_product.setText("Producto nuevo");
        register_product.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                register_productActionPerformed(evt);
            }
        });
        stock.add(register_product, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 190, -1, 30));

        registered_product.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        registered_product.setText("Producto registrado");
        registered_product.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registered_productActionPerformed(evt);
            }
        });
        stock.add(registered_product, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 190, -1, 30));

        availables_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        availables_label.setText("Disponibles:");
        stock.add(availables_label, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 230, 70, 30));

        availables.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        availables.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                availablesKeyTyped(evt);
            }
        });
        stock.add(availables, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 230, 60, 30));

        update.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        update.setText("Actualizar");
        update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateActionPerformed(evt);
            }
        });
        stock.add(update, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 190, -1, 30));

        product_name_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        product_name_label.setText("Nombre:");
        stock.add(product_name_label, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 230, 60, 30));

        product_name.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        product_name.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                product_nameKeyTyped(evt);
            }
        });
        stock.add(product_name, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 230, 140, 30));

        registered_products.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        registered_products.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "(Seleccionar)" }));
        registered_products.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                registered_productsItemStateChanged(evt);
            }
        });
        registered_products.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registered_productsActionPerformed(evt);
            }
        });
        stock.add(registered_products, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 190, 150, 30));

        price_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        price_label.setText("Precio:");
        stock.add(price_label, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 230, 50, 30));

        price.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        price.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                priceKeyTyped(evt);
            }
        });
        stock.add(price, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 230, 80, 30));

        products_list_s.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        products_list_s.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nombre", "Precio", "Disponibles", "Reservados"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        products_list_s.setEnabled(false);
        scroll_pane_st.setViewportView(products_list_s);

        stock.add(scroll_pane_st, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 11, 660, 170));

        reserved.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        reserved.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                reservedKeyTyped(evt);
            }
        });
        stock.add(reserved, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 230, 60, 30));

        reserved_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        reserved_label.setText("Reservados:");
        stock.add(reserved_label, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 230, 70, 30));

        tabbed_pane.addTab("Productos en inventario", stock);

        getContentPane().add(tabbed_pane, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 700, 300));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try { 
           Runtime.getRuntime().exec ("C:\\xampp\\xampp_stop.exe"); 
        } 
        catch (Exception e) {}
    }//GEN-LAST:event_formWindowClosing

    private void reports_sincePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_reports_sincePropertyChange
        reports_until.setMinSelectableDate(reports_since.getDate());
    }//GEN-LAST:event_reports_sincePropertyChange

    private void reports_untilPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_reports_untilPropertyChange
        reports_since.setMaxSelectableDate(reports_until.getDate());
    }//GEN-LAST:event_reports_untilPropertyChange

    private void payment_method_aActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_payment_method_aActionPerformed

    }//GEN-LAST:event_payment_method_aActionPerformed

    private void client_name_aKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_client_name_aKeyTyped
        char ch = evt.getKeyChar();
        if ((ch != ' ') && (ch < 'A' || ch > 'Z') && (ch < 'a' || ch > 'z'))
            evt.consume();
    }//GEN-LAST:event_client_name_aKeyTyped

    private void priceKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_priceKeyTyped
        char ch = evt.getKeyChar();
        if (ch < '0' || ch > '9')
            evt.consume();
    }//GEN-LAST:event_priceKeyTyped

    private void registered_productsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registered_productsActionPerformed
        addRegisteredProduct();
    }//GEN-LAST:event_registered_productsActionPerformed

    private void registered_productsItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_registered_productsItemStateChanged

    }//GEN-LAST:event_registered_productsItemStateChanged

    private void product_nameKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_product_nameKeyTyped
        
    }//GEN-LAST:event_product_nameKeyTyped

    private void updateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateActionPerformed
        if (product_name.getText().equals("") || price.getText().equals("") || availables.getText().equals("") || reserved.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Faltan datos para actualizar el producto");
        } else {
            if (registered_products.isEnabled())
                updateRegisteredProduct();
            else
                registerNewProduct();

            JOptionPane.showMessageDialog(null, "Producto actualizado correctamente");
            showStock();
            initReserve();
            lock(update,registered_products,availables,price,product_name);
        }
    }//GEN-LAST:event_updateActionPerformed

    private void availablesKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_availablesKeyTyped
        char ch = evt.getKeyChar();
        if (ch < '0' || ch > '9')
            evt.consume();
    }//GEN-LAST:event_availablesKeyTyped

    private void registered_productActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registered_productActionPerformed
        unlock(update,availables,reserved,price,registered_products);
        lock(product_name);
        registered_products.setSelectedIndex(0);
        price.setText("");
        availables.setText("");
        product_name.setText("");
    }//GEN-LAST:event_registered_productActionPerformed

    private void register_productActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_register_productActionPerformed
        unlock(availables,price,product_name,update);
        lock(registered_products);
        registered_products.setSelectedIndex(0);
        price.setText("");
        availables.setText("");
        reserved.setText("");
        product_name.setText("");
    }//GEN-LAST:event_register_productActionPerformed

    private void payment_number_rKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_payment_number_rKeyTyped
        char ch = evt.getKeyChar();
        if ((ch < '0' || ch > '9') && ch != ' ' && ch != ',')
            evt.consume();
    }//GEN-LAST:event_payment_number_rKeyTyped

    private void cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelActionPerformed
        initReserve();
    }//GEN-LAST:event_cancelActionPerformed

    private void reserveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reserveActionPerformed
        if (payment_method_r.getSelectedIndex() == 0 || client_name_r.getText().equals("") || products_list_r.getRowCount() == 0)
            JOptionPane.showMessageDialog(null, "Datos insuficientes para realizar la venta");
        else {
            registerOrCancelReserve();
        }
    }//GEN-LAST:event_reserveActionPerformed

    private void addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addActionPerformed
        addProductToReserve();
        afterAddProductToReserve();
        assignTotal();
        unlock(reserve,cancel);
    }//GEN-LAST:event_addActionPerformed

    private void products_combobox_rActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_products_combobox_rActionPerformed
        if (products_combobox_r.getItemCount() != 0) {
            if (products_combobox_r.getSelectedIndex() == 0)
                lock(add);
            else
                unlock(add);
        }
    }//GEN-LAST:event_products_combobox_rActionPerformed

    private void client_name_rKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_client_name_rKeyTyped
        char ch = evt.getKeyChar();
        if ((ch != ' ') && (ch < 'A' || ch > 'Z') && (ch < 'a' || ch > 'z'))
            evt.consume();
    }//GEN-LAST:event_client_name_rKeyTyped

    private void transfers_sincePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_transfers_sincePropertyChange
        transfers_until.setMinSelectableDate(transfers_since.getDate());
    }//GEN-LAST:event_transfers_sincePropertyChange

    private void transfers_untilPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_transfers_untilPropertyChange
        transfers_since.setMaxSelectableDate(transfers_until.getDate());
    }//GEN-LAST:event_transfers_untilPropertyChange

    private void search_for_fileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_search_for_fileActionPerformed
        String file = "";
        try {
            JFileChooser jfc = new JFileChooser();
            jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
            jfc.setFileFilter(new FileNameExtensionFilter("*.pdf", "pdf"));
            jfc.showSaveDialog(null);

            file = jfc.getSelectedFile().getAbsolutePath();
        } catch (Exception e) {}
        
        if (!file.equals(""))
            searchPaymentIds(file);
    }//GEN-LAST:event_search_for_fileActionPerformed

    private void search_for_transfers_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_search_for_transfers_buttonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_search_for_transfers_buttonActionPerformed

    private void search_reportsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_search_reportsActionPerformed
        showReport();
    }//GEN-LAST:event_search_reportsActionPerformed

    private void order_idsKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_order_idsKeyTyped
        char ch = evt.getKeyChar();
        if ((ch < '0' || ch > '9') && ch != ' ' && ch != ',')
            evt.consume();
    }//GEN-LAST:event_order_idsKeyTyped

    private void delete_reserveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_delete_reserveActionPerformed
        if (!order_ids.getText().equals("")) {
            deleteReserves();
        } else {
            JOptionPane.showMessageDialog(null, "No ha seleccionado órdenes para eliminar");
        }
    }//GEN-LAST:event_delete_reserveActionPerformed

    private void confirm_sale_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirm_sale_buttonActionPerformed
        if (!order_ids.getText().equals("")) {
            confirmReserves();
        } else {
            JOptionPane.showMessageDialog(null, "No ha seleccionado órdenes para eliminar");
        }
    }//GEN-LAST:event_confirm_sale_buttonActionPerformed

    private void payment_number_sKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_payment_number_sKeyTyped
        char ch = evt.getKeyChar();
        if ((ch < '0' || ch > '9') && ch != ' ' && ch != ',')
            evt.consume();
    }//GEN-LAST:event_payment_number_sKeyTyped

    private void order_idsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_order_idsActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_order_idsActionPerformed

    private void reservedKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_reservedKeyTyped
        char ch = evt.getKeyChar();
        if (ch < '0' || ch > '9')
            evt.consume();
    }//GEN-LAST:event_reservedKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Rafmar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Rafmar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Rafmar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Rafmar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new Rafmar().setVisible(true);
                } catch (IOException ex) {
                    Logger.getLogger(Rafmar.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton add;
    private javax.swing.JTextField availables;
    private javax.swing.JLabel availables_label;
    private javax.swing.JButton cancel;
    private javax.swing.JLabel client_label_a;
    private javax.swing.JLabel client_label_r;
    private javax.swing.JTextField client_name_a;
    private javax.swing.JTextField client_name_r;
    private javax.swing.JPanel confirm_sale;
    private javax.swing.JButton confirm_sale_button;
    private javax.swing.JCheckBox confirm_sale_r;
    private javax.swing.JButton delete_reserve;
    private javax.swing.JTable found_transfers;
    private javax.swing.JLabel order_id_label;
    private javax.swing.JTextField order_ids;
    private javax.swing.JLabel payment_label_r;
    private javax.swing.JLabel payment_label_s;
    private javax.swing.JComboBox<String> payment_method_a;
    private javax.swing.JComboBox<String> payment_method_r;
    private javax.swing.JTextField payment_number_r;
    private javax.swing.JTextField payment_number_s;
    private javax.swing.JTextField price;
    private javax.swing.JLabel price_label;
    private javax.swing.JTextField product_name;
    private javax.swing.JLabel product_name_label;
    private javax.swing.JComboBox<String> products_combobox_r;
    private javax.swing.JTable products_list_r;
    private javax.swing.JTable products_list_s;
    private javax.swing.JLabel quantity_label_r;
    private javax.swing.JSpinner quantity_r;
    private javax.swing.JButton register_product;
    private javax.swing.JButton registered_product;
    private javax.swing.JComboBox<String> registered_products;
    private javax.swing.JPanel reports;
    private com.toedter.calendar.JDateChooser reports_since;
    private com.toedter.calendar.JDateChooser reports_until;
    private javax.swing.JButton reserve;
    private javax.swing.JTextField reserved;
    private javax.swing.JLabel reserved_label;
    private javax.swing.JPanel reserves;
    private javax.swing.JTable sales_list;
    private javax.swing.JTable sales_table;
    private javax.swing.JScrollPane scroll_pane_a;
    private javax.swing.JScrollPane scroll_pane_r;
    private javax.swing.JScrollPane scroll_pane_sa;
    private javax.swing.JScrollPane scroll_pane_st;
    private javax.swing.JScrollPane scroll_pane_t;
    private javax.swing.JButton search_for_file;
    private javax.swing.JPanel search_for_transfers;
    private javax.swing.JButton search_for_transfers_button;
    private javax.swing.JButton search_reports;
    private javax.swing.JPanel stock;
    private javax.swing.JTabbedPane tabbed_pane;
    private javax.swing.JLabel total;
    private com.toedter.calendar.JDateChooser transfers_since;
    private javax.swing.JLabel transfers_since_label;
    private com.toedter.calendar.JDateChooser transfers_until;
    private javax.swing.JLabel transfers_until_label;
    private javax.swing.JButton update;
    // End of variables declaration//GEN-END:variables

    void lock(java.awt.Component... c) {
        for (java.awt.Component i : c) {
            i.setEnabled(false);
        }
    }
    
    void unlock(java.awt.Component... c) {
        for (java.awt.Component i : c) {
            i.setEnabled(true);
        }
    }
    
    void initReserve() {
        client_name_r.setText("");
        payment_number_r.setText("");
        payment_method_r.setSelectedIndex(0);
        total.setText("Total: Bs. S ");
        lock(reserve, cancel);
        confirm_sale_r.setSelected(false);
        
        DefaultTableModel dtm = (DefaultTableModel)products_list_r.getModel();
        int rowCount = dtm.getRowCount();
        for (int i = rowCount - 1; i >= 0; i--) {
            dtm.removeRow(i);
        }
        
        afterAddProductToReserve();
    }
    
    void afterAddProductToReserve() {
        quantity_r.setValue(1);
        products_combobox_r.removeAllItems();
        products_combobox_r.addItem("Productos");
        
        try {
            String sql = "SELECT name FROM products WHERE id IN (SELECT product_id FROM stock WHERE availables > 0)";
            Statement s = cn.createStatement();
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                products_combobox_r.addItem(rs.getString(1));
            }
        } catch (Exception e) {}
        
        DefaultTableModel dtm = (DefaultTableModel)products_list_r.getModel();
        int rowCount = dtm.getRowCount();
        for (int i = 0; i < rowCount; i++) {
            Object product_name = dtm.getValueAt(i, 0);
            products_combobox_r.removeItem(product_name);
        }
    }
    
    void addProductToReserve() {
        try {
            String sql = "SELECT p.name, s.availables, p.price FROM products p INNER JOIN stock s ON (p.id = s.product_id) WHERE p.name = '" + (String)products_combobox_r.getSelectedItem() + "'";
            Statement s = cn.createStatement();
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                String nombre = (String)products_combobox_r.getSelectedItem();
                int quantity = (int)quantity_r.getValue();
                int unit_price = rs.getInt(3);
                int total_price = quantity * unit_price;
                
                if (quantity > rs.getInt(2))
                    JOptionPane.showMessageDialog(null, "Cantidad seleccionada sobrepasa el máximo en inventario");
                else {
                    DefaultTableModel dtm = (DefaultTableModel)products_list_r.getModel();
                    dtm.addRow(new Object[]{nombre, String.valueOf(unit_price), String.valueOf(quantity), String.valueOf(total_price)});
                }
            }
        } catch (Exception e) {}
    }
    
    void assignTotal() {
        int total = 0;
        
        DefaultTableModel dtm = (DefaultTableModel)products_list_r.getModel();
        int rowCount = dtm.getRowCount();
        for (int i = 0; i < rowCount; i++)
            total += Integer.valueOf(String.valueOf(dtm.getValueAt(i,3)));
        
        this.total.setText("Total: Bs. S " + total);
    }
    
    void updateRegisteredProduct() {
        try {
            PreparedStatement ps = cn.prepareStatement("UPDATE stock s INNER JOIN products p ON (s.product_id = p.id) SET s.availables = " + availables.getText() + ", s.reserved = " + reserved.getText() + ", p.price = " + price.getText() + " WHERE p.name = '" + (String)registered_products.getSelectedItem() + "'");
            ps.executeUpdate();
        } catch (SQLException e) {}
    }
    
    void registerNewProduct() {
        try {
            PreparedStatement ps = cn.prepareStatement("INSERT INTO products(name,price) VALUES('" + product_name.getText() + "'," + price.getText() + ")");
            ps.executeUpdate();
            ps = cn.prepareStatement("INSERT INTO stock(product_id,availables,reserved) VALUES((SELECT MAX(id) FROM products)," + availables.getText() + "," + reserved.getText() + ")");
            ps.executeUpdate();
        } catch (SQLException e) {}
    }
    
    void addRegisteredProduct() {
        if (registered_products.getItemCount() != 0 && registered_products.getSelectedIndex() != 0) {
            try {
                String sql = "SELECT p.name, s.availables, p.price, s.reserved FROM products p INNER JOIN stock s ON (p.id = s.product_id) WHERE p.name = '" + (String)registered_products.getSelectedItem() + "'";
                Statement s = cn.createStatement();
                ResultSet rs = s.executeQuery(sql);            
                while (rs.next()) {
                    product_name.setText(rs.getString(1));
                    availables.setText(rs.getString(2));
                    price.setText(rs.getString(3));
                    reserved.setText(rs.getString(4));
                }
            } catch (Exception e) {}
        } else {
            product_name.setText("");
            price.setText("");
            availables.setText("");
            reserved.setText("");
        }
    }
    
    void showStock() {
        registered_products.removeAllItems();
        registered_products.addItem("(Seleccionar)");
        DefaultTableModel dtm = (DefaultTableModel)products_list_s.getModel();
        int rowCount = dtm.getRowCount();
        for (int i = rowCount - 1; i >= 0; i--)
            dtm.removeRow(i);
        
        try {
            String sql = "SELECT p.name, p.price, s.availables, s.reserved FROM products p INNER JOIN stock s ON (p.id = s.product_id)";
            Statement s = cn.createStatement();
            ResultSet rs = s.executeQuery(sql);            
            while (rs.next()) {
                dtm.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4)});
                registered_products.addItem(rs.getString(1));
            }
        } catch (Exception e) {}
    }
    
    void addReserveToDatabase() {
        String table, columns, values;
        
        if (payment_method_r.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un método de pago antes de registrar la venta o reserva");
        } else {
            if (confirm_sale_r.isSelected()) {
                table = "sales";
            } else {
                table = "reserves";
            }
            
            SimpleDateFormat sdt = new SimpleDateFormat("yyyy-MM-dd");
            try {
                PreparedStatement ps = cn.prepareStatement("INSERT INTO total_" + table + " (day,user_name,price,payment_method) VALUES(CURDATE(),'" + client_name_r.getText() + "'," + total.getText().replace("Total: Bs. S ", "") + ",(SELECT id FROM payment_methods WHERE name = '" + (String)payment_method_r.getSelectedItem() + "'))");
                ps.executeUpdate();
                
                if (confirm_sale_r.isSelected()) {
                    String sql = "INSERT INTO payment_numbers (sale_id,payment_number) VALUES";
                    String[] payment_numbers = payment_number_r.getText().replace(" ", "").split(",");
                    for (int i = 0; i < payment_numbers.length; i++) {
                        sql += " ((SELECT MAX(id) FROM total_sales)," + payment_numbers[i] + "),"; 
                    }
                    sql = sql.substring(0, sql.length() - 1);
                    ps = cn.prepareStatement(sql);
                    ps.executeUpdate();
                }
            } catch (SQLException e) {}

            DefaultTableModel dtm = (DefaultTableModel)products_list_r.getModel();
            int rowCount = dtm.getRowCount();
            for (int i = rowCount - 1; i >= 0; i--) {
                try {
                    PreparedStatement ps = cn.prepareStatement("UPDATE stock SET availables = availables - " + String.valueOf(dtm.getValueAt(i,2)) + " WHERE product_id = (SELECT id FROM products WHERE name = '" + String.valueOf(dtm.getValueAt(i,0)) + "')");
                    ps.executeUpdate();
                    ps = cn.prepareStatement("INSERT INTO individual_" + table + " (product_id,unit_price,quantity,total_price,belongs_to) VALUES ((SELECT id FROM products WHERE name = '" + String.valueOf(dtm.getValueAt(i,0)) + "')," + String.valueOf(dtm.getValueAt(i,1)) + "," + String.valueOf(dtm.getValueAt(i,2)) + "," + String.valueOf(dtm.getValueAt(i,3)) + ", (SELECT MAX(id) FROM total_" + table + "))");
                    ps.executeUpdate();

                    if (!confirm_sale_r.isSelected()) {
                        ps = cn.prepareStatement("UPDATE stock SET reserved = reserved + " + String.valueOf(dtm.getValueAt(i,2)) + " WHERE product_id = (SELECT id FROM products WHERE name = '" + String.valueOf(dtm.getValueAt(i,0)) + "')");
                        ps.executeUpdate();
                    }
                } catch (SQLException e) {}

                dtm.removeRow(i);
            }
            if (confirm_sale_r.isSelected()) {
                JOptionPane.showMessageDialog(null, "Venta registrada con éxito");
            } else {
                JOptionPane.showMessageDialog(null, "Reserva registrada con éxito");
            }
            initReserve();
            showStock();
            showReport();
        }
    }
    
    void showReport() {
        DefaultTableModel dtm = (DefaultTableModel)sales_table.getModel();
        int rowCount = dtm.getRowCount();
        for (int i = rowCount - 1; i >= 0; i--) {
            dtm.removeRow(i);
        }
        SimpleDateFormat sdt = new SimpleDateFormat("yyyy-MM-dd");
        
        String conditions = "WHERE table.user_name LIKE '%" + client_name_a.getText() + "%'";
        if (payment_method_a.getSelectedIndex() != 0)
            conditions += " AND table.payment_method = (SELECT id FROM payment_methods WHERE name = '" + (String)payment_method_a.getSelectedItem() + "')";
        if (reports_since.getDate() != null && reports_until.getDate() != null) {
            conditions += " AND (table.day BETWEEN '" + sdt.format(reports_since.getDate()) + "' AND '" + sdt.format(reports_until.getDate()) + "')";
        } else if (reports_until.getDate() != null) {
            conditions += " AND table.day <= '" + sdt.format(reports_until.getDate()) + "'";
        } else if (reports_since.getDate() != null) {
            conditions += " AND table.day >= '" + sdt.format(reports_since.getDate()) + "'";
        }
        
        try {
            String sql = "(SELECT ts.day, ts.user_name, ts.price, ts.payment_method, pn.payment_number, 'Venta' FROM total_sales ts LEFT JOIN payment_numbers pn ON (ts.id = pn.sale_id)" + conditions.replace("table.", "ts.") + ") UNION (SELECT day, user_name, price, payment_method, 0, 'Reserva' FROM total_reserves " + conditions.replace("table.", "") + ") ORDER BY day DESC";
            Statement s = cn.createStatement();
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                dtm.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6)});
            }
        } catch (Exception e) {}
        
        showReserves();
    }
    
    void searchPaymentIds(String file) {
        DefaultTableModel dtm = (DefaultTableModel)found_transfers.getModel();
        int rowCount = dtm.getRowCount();
        for (int i = rowCount - 1; i >= 0; i--) {
            dtm.removeRow(i);
        }
        SimpleDateFormat sdt = new SimpleDateFormat("yyyy-MM-dd");
        
        String conditions = "WHERE payment_method = (SELECT id FROM payment_methods WHERE name = 'Transferencia')";
        if (transfers_since.getDate() != null && transfers_until.getDate() != null) {
            conditions += " AND (day BETWEEN '" + sdt.format(transfers_since.getDate()) + "' AND '" + sdt.format(transfers_until.getDate()) + "')";
        } else if (transfers_since.getDate() != null) {
            conditions += " AND day >= '" + sdt.format(transfers_since.getDate()) + "'";
        } else if (transfers_until.getDate() != null) {
            conditions += " AND day <= '" + sdt.format(transfers_until.getDate()) + "'";
        }
        
        try {
            String sql = "SELECT GROUP_CONCAT(DISTINCT ts.day SEPARATOR ', ') AS days_sales, GROUP_CONCAT(DISTINCT ts.user_name SEPARATOR ', ') AS users_name, pn.payment_number AS payment_id, SUM(ts.price) AS sales_price FROM payment_numbers pn INNER JOIN total_sales ts ON (pn.sale_id = ts.id) " + conditions + " GROUP BY pn.payment_number ORDER BY users_name, ts.day ASC";
            Statement s = cn.createStatement();
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                String payment_id = rs.getString(3);
                PdfReader reader = new PdfReader(file);
                int pages = reader.getNumberOfPages(); 
                boolean found = false;

                for (int i = 1; i <= pages; i++) { 
                    String page = PdfTextExtractor.getTextFromPage(reader, i);
                    String[] lines = page.split("\n");
                    
                    for (int j = 0; j < lines.length; j++) {
                        String[] fields = lines[j].split(" ");
                        
                        if (fields.length - 2 > 0 && fields[1].length() >= payment_id.length() && fields[1].substring(fields[1].length()-payment_id.length()).equals(payment_id) && fields[0].substring(6).equals(String.valueOf(new Date().getYear() + 1900))) {
                            dtm.addRow(new Object[]{rs.getString(1), fields[0], rs.getString(2), fields[1], rs.getString(4), fields[fields.length - 2]});
                            found = true;
                        } else if (fields.length - 3 > 0 && fields[1].length() >= payment_id.length() && fields[1].substring(fields[1].length()-payment_id.length()).equals(payment_id)) {
                            dtm.addRow(new Object[]{rs.getString(1), "Hoy", rs.getString(2), fields[1], rs.getString(4), fields[fields.length - 3]});
                            found = true;
                        }
                    }
                }
                
                if (!found)
                    dtm.addRow(new Object[]{rs.getString(2), "No encontrada", rs.getString(3), payment_id, rs.getString(4), "No encontrada"});                

                reader.close();
            }
        } catch (Exception e) {}
    }
    
    void setPaymentMethods() {
        payment_method_r.addItem("Método de pago");
        payment_method_a.addItem("Método de pago");
        try {
            ResultSet query = cn.createStatement().executeQuery("SELECT * FROM payment_methods");
            while (query.next()) {
                payment_method_r.addItem(query.getString(2));
                payment_method_a.addItem(query.getString(2));
            }
        } catch (Exception e) {}
    }
    
    void registerOrCancelReserve() {
        SimpleDateFormat sdt = new SimpleDateFormat("dd/MM/yyyy");
        
        try {
            String sql = "SELECT p.name, SUM(ir.quantity), NULL, NULL FROM total_reserves tr INNER JOIN individual_reserves ir ON (tr.id = ir.belongs_to) INNER JOIN products p ON (ir.product_id = p.id) WHERE tr.user_name = '" + client_name_r.getText() + "' GROUP BY name UNION SELECT DISTINCT NULL, NULL, day, NULL FROM total_reserves WHERE user_name = '" + client_name_r.getText() + "' UNION SELECT NULL, NULL, NULL, SUM(price) FROM total_reserves WHERE user_name = '" + client_name_r.getText() + "'";
            Statement s = cn.createStatement();
            ResultSet rs = s.executeQuery(sql);
            if (rs.next() && rs.next()) {
                rs.previous(); rs.previous();
                String text = "El usuario proporcionado posee reservas pendientes:\n";
                while (rs.next()) {
                    if (rs.getString(1) != null && rs.getString(2) != null) {
                        text += rs.getString(1) + " (" + rs.getString(2) + ")\n";
                    }
                    if (rs.getString(3) != null) {
                        text += "El día " + sdt.format(rs.getDate(3)) + "\n";
                    }
                    if (rs.getString(4) != null) {
                        text += "Para un total de Bs.S " + rs.getString(4);
                    }
                }
                if (confirm_sale_r.isSelected()) {
                    text += "\n¿Está seguro de que desea generar la venta actual?";
                } else {
                    text += "\n¿Está seguro de que desea generar la reserva actual?";
                }
                
                int answer = JOptionPane.showConfirmDialog(this, text, "Importante", JOptionPane.YES_NO_CANCEL_OPTION);
                if (answer == 0)
                    addReserveToDatabase();
                else if (answer == 1)
                    initReserve();
            } else {
                addReserveToDatabase();
            }
        } catch (Exception e) {}
    }
    
    void showReserves() {
        DefaultTableModel dtm = (DefaultTableModel)sales_list.getModel();
        int rowCount = dtm.getRowCount();
        for (int i = rowCount - 1; i >= 0; i--)
            dtm.removeRow(i);
        
        try {
            Statement s = cn.createStatement();
            ResultSet rs = s.executeQuery("SELECT tr.id, tr.day, tr.user_name, pm.name, GROUP_CONCAT(CONCAT((SELECT name FROM products WHERE id = ir.product_id), ' (', ir.quantity, ')') SEPARATOR ', '), tr.price FROM total_reserves tr INNER JOIN individual_reserves ir ON (tr.id = ir.belongs_to) LEFT JOIN payment_methods pm ON (tr.payment_method = pm.id) GROUP BY tr.id");
            while (rs.next()) {
                dtm.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6)});
            }
        } catch (Exception e) {}
        
    }
    
    void deleteReserves() {
        try {
            PreparedStatement ps = cn.prepareStatement("DELETE FROM individual_reserves WHERE belongs_to IN (" + order_ids.getText() + ")");
            ps.executeUpdate();
            ps = cn.prepareStatement("DELETE FROM total_reserves WHERE id IN (" + order_ids.getText() + ")");
            ps.executeUpdate();
        } catch (SQLException e) {}
        order_ids.setText("");
        payment_number_s.setText("");
        showReserves();
        JOptionPane.showMessageDialog(null, "Ha eliminado correctamente las reservas");
    }
    
    void confirmReserves() {
        String[] reserve_ids = order_ids.getText().replace(" ", "").split(",");
        String[] payment_numbers = payment_number_s.getText().replace(" ", "").split(",");
        
        for (int i = 0; i < reserve_ids.length; i++) {
            try {
                PreparedStatement ps = cn.prepareStatement("INSERT INTO total_sales (day, user_name, price, payment_method) SELECT day, user_name, price, payment_method FROM total_reserves WHERE id = " + reserve_ids[i]);
                ps.executeUpdate();
                ps = cn.prepareStatement("INSERT INTO individual_sales (product_id, unit_price, quantity, total_price, belongs_to) SELECT product_id, unit_price, quantity, total_price, (SELECT MAX(id) FROM total_sales) FROM individual_reserves WHERE belongs_to = " + reserve_ids[i]);
                ps.executeUpdate();
                ps = cn.prepareStatement("DELETE FROM individual_reserves WHERE belongs_to = " + reserve_ids[i]);
                ps.executeUpdate();
                ps = cn.prepareStatement("DELETE FROM total_reserves WHERE id = " + reserve_ids[i]);
                ps.executeUpdate();
                

                if (payment_numbers != null) {
                    String sql = "INSERT INTO payment_numbers (sale_id,payment_number) VALUES";
                    for (int j = 0; j < payment_numbers.length; j++) {
                        sql += " ((SELECT MAX(id) FROM total_sales)," + payment_numbers[j] + "),";
                    }
                    sql = sql.substring(0, sql.length() - 1);
                    ps = cn.prepareStatement(sql);
                    ps.executeUpdate();
                }
            } catch (Exception e) {}
        }
        order_ids.setText("");
        payment_number_s.setText("");
        JOptionPane.showMessageDialog(null, "Ha confirmado correctamente las ventas");
        showReserves();
    }
}
