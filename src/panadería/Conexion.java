package panadería;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.sql.*;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

class Conexion {
    Connection cn;
    
    public Connection connect() throws IOException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/rafmar", "root", "");
        } catch(Exception e) {
            JOptionPane.showMessageDialog(null, "Hubo un problema al intentar conectar con la base de datos. Intente abrir la aplicación luego.");
            Runtime.getRuntime().exec("C:\\xampp\\xampp_stop.exe"); 
            System.exit(0);
        }
        return cn;
    }
    
    Statement createStatement() {
        throw new UnsupportedOperationException("No soportado");
    }
}
